import { links, linksIncrust, project, right } from "./const";
let Liens:HTMLElement[] = []


for (let i = 0; i < links.length; i++) {
    const element:HTMLElement = document.createElement('p')
    element.textContent = links[i].title
    Liens.push(element)
    element.onclick = (e) => subTitle(i, element)
    linksIncrust.appendChild(element)
    element.addEventListener('click', () => {
        display(links[i].title);    
    })
}



function display(myName: string) {
    right.innerHTML = ""
    const element = newElement('div');
    element.id = myName + 'Div'
    right.appendChild(element);
    createContent(element)
}

function newElement(balise: string, src?: string, href?: string, content?: string) {
    if (src && balise == 'img') {
        const img = new Image()
        img.src = src
        img.classList.add('project-image')
        return img;
    }
    if (href && balise == 'a') {
        const a = document.createElement('a')
        a.href = href
        a.textContent = content
        return a;
    }
    if (content) {
        const p = document.createElement(balise)
        p.textContent = content
        return p;
    }
    const element = document.createElement(balise)
    return element
}

function createContent(element: HTMLElement) {
    switch (element.id) {
        case links[1].title + "Div":
            project.forEach(index => {
                let newProjectElement: any = [
                    newElement('img', index.image),
                    newElement('h3', null, null, index.title),
                    newElement('p', null, null, index.des)
                ]
                let myDiv = newElement('div')
                myDiv.className = 'mesProjets'
                newProjectElement.forEach((i: any) => {
                    myDiv.appendChild(i)
                });
                element.appendChild(myDiv)
                myDiv.addEventListener("mouseover", () => {
                    newProjectElement.forEach((i: any) => {
                        i.style.display = "block"
                    })
                });
                myDiv.addEventListener("mouseout", () => {
                    newProjectElement[1].style.display = "none"
                    newProjectElement[2].style.display = "none"
                })
            });
            
            break;
        case links[2].title + "Div":

            break;
        case links[3].title + "Div":

            break;

        default:
            break;
    }
}



function subTitle(i:number, element:HTMLElement){
    element.classList.add("yes")
    
    for (let index = 0; index < Liens.length; index++) {
        if (index!= i) {
            Liens[index].classList.remove("yes")
        }
    }

}